package com.ske.snakebaddesign.models;

import java.util.Random;

/**
 * Created by Momo on 15/3/2559.
 */
public class Dice {
    private int face;

    public Dice(){
        face = 1;
    }

    public void RandomFace(){
        face =  1+new Random().nextInt(6);
    }

    public int getFace(){
        return face;
    }
}
