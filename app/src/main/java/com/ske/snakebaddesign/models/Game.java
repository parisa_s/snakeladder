package com.ske.snakebaddesign.models;

/**
 * Created by Momo on 16/3/2559.
 */
public class Game {
    private static Game game;
    private Player player1;
    private Player player2;
    private Board board;
    private Dice dice;
    private int turn;

    private Game() {
        board = Board.getInstance();
        dice = new Dice();
        player1 = new Player("Player1");
        player2 = new Player("Player2");
    }

    public static Game getInstance(){
        if(game == null){
            game = new Game();
        }
        return game;
    }
    public void resetGame(){
        player1.setPosition(0);
        player2.setPosition(0);
        turn = 0;
        board.setSize(6);
    }

    public int gettakeTurn(){
        dice.RandomFace();
        int val = dice.getFace();
        return val;
    }

    public int computePosition(Player player,int value){
        int current  = player.getPosition()+value;
        int maxSquare = board.getSize() * board.getSize() - 1;
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        player.setPosition(current);
        return current;
    }
    public void checkEffect(Board bod,Player player){
        Square sq = bod.getSquare(player.getPosition());
        sq.effect(player);
    }
    public Player getPlayer1(){
        return this.player1;
    }

    public Player getPlayer2(){
        return this.player2;
    }

    public void setTurn(int turn){
        this.turn = turn;
    }

    public Board getBoard(){
        return board;
    }
}
