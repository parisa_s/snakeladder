package com.ske.snakebaddesign.models;

/**
 * Created by Momo on 15/3/2559.
 */
public interface Square {
    public void effect(Player player);
    public int getColor();
}
