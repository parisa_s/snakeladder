package com.ske.snakebaddesign.models;

import android.graphics.Color;

/**
 * Created by Momo on 16/3/2559.
 */
public class WarpSquare implements Square{
    private Board board;
    private int colorCell ;

    public WarpSquare(){
        colorCell = Color.parseColor("#25aa4c");
    }
    @Override
    public void effect(Player player) {
        player.setPosition((int)Math.floor(Math.random()*Board.getInstance().getSize()*Board.getInstance().getSize()) );
    }
    public int getColor(){
        return colorCell;
    }
}
