package com.ske.snakebaddesign.models;

import android.graphics.Color;

/**
 * Created by Momo on 15/3/2559.
 */
public class NormalSquare implements Square {
    private int colorCell ;

    public NormalSquare(){
        colorCell = Color.parseColor("#87aa4c");
    }
    @Override
    public void effect(Player player) {

    }

    public int getColor(){
        return colorCell;
    }
}
