package com.ske.snakebaddesign.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Momo on 15/3/2559.
 */
public class Board {
    private static Board board;
    private int size;
    private List<Square> squares;

    private Board(int size){
        this.size = size;
        squares = new ArrayList<>();
        initSquare();
    }

    public static Board getInstance(){
        if(board == null){
            board = new Board(6);
        }
        return board;
    }
    public void initSquare(){
        for(int i=0;i<size*size;i++){
            if(i == 29 || i == 7 || i==20){
            //if(i > 6 && i < 13)
                squares.add(new WarpSquare());
            }else{
                squares.add(new NormalSquare());
            }
        }
    }

    public Square getSquare(int position){
        return squares.get(position);
    }
    public void setSize(int size){
        this.size = size;
    }

    public int getSize(){
        return size;
    }

}
