package com.ske.snakebaddesign.models;

/**
 * Created by Momo on 15/3/2559.
 */
public class Player {
    private String name;
    private int position;

    public Player(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public int getPosition(){
        return position;
    }

    public void setPosition(int position){
        this.position = position;
    }
}
